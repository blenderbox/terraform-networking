output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "public_subnets_ids" {
  value = concat(aws_subnet.public_subnet.*.id)
}

output "public_subnets_cidr" {
  value = concat(aws_subnet.public_subnet.*.cidr_block)
}

output "private_subnets_ids" {
  value = concat(aws_subnet.private_subnet.*.id)
}

output "private_subnets_cidr" {
  value = concat(aws_subnet.private_subnet.*.cidr_block)
}

output "vpc_security_group_id" {
  value = aws_vpc.vpc.default_security_group_id
}

output "default_sg_id" {
  value = aws_security_group.default.id
}

output "http_access_sg_id" {
  value = aws_security_group.http_access_sg.id
}

output "jumpserver_ssh_id" {
  value = aws_security_group.jumpserver_ssh.id
}

output "security_groups_ids" {
  value = [aws_security_group.default.id, aws_security_group.jumpserver_ssh.id]
}
